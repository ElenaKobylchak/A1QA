import file_utils.FileUtils;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

public class PageTest {

    private UserData user;
    private String fileName;
    private AuthPage authPage;
    private PageUtils pageUtils;
    private MainPage mainPage;
    private String url;

    @BeforeTest
    public void readConfigsAndTestData() {
        try {
            JSONObject testData = (JSONObject) FileUtils.readJSON("test_data.json").get(0);
            user = new UserData((String) testData.get("login"), (String) testData.get("password"));
            fileName = (String) testData.get("filename");
            url =  (String) FileUtils.readConfig("config.json", "url");
            new BrowserFactory((String)FileUtils.readConfig("config.json", "browser")).createBrowser();
            PageUtils.setPollingSeconds((Long) testData.get("polling_seconds"));
            PageUtils.setTimeoutSeconds((Long) testData.get("timeout_seconds"));
            pageUtils = new PageUtils();

        } catch (Exception ex) {
            System.err.println(ex);
        }
    }

    @BeforeTest
    public void readLocators() {
        try {
            final JSONObject locators = (JSONObject) FileUtils.readJSON("locators.json").get(0);

            authPage = new AuthPage(user, new ArrayList<String>()
            {{
                add((String) locators.get("LOGIN_LABEL"));
                add((String) locators.get("LOGIN_LOCATOR"));
                add((String) locators.get("PASSWORD_LOCATOR"));
                add((String) locators.get("FIELD_FILLED"));
                add((String) locators.get("PASSPORT_LOCATOR"));
            }});

            mainPage = new MainPage(new ArrayList<String>()
            {{
                add((String) locators.get("MARKET_APP_TAG"));
                add((String) locators.get("DISCOUNT_TAG"));
                add((String) locators.get("GOODS_BUTTON"));
                add((String) locators.get("ALL_GOODS"));
                add((String) locators.get("DOSUG"));
                add((String) locators.get("POPULAR_GOODS"));
                add((String) locators.get("LOGOUT_FORM"));
                add((String) locators.get("LOGOUT_BUTTON"));
                add((String) locators.get("POP_UP_WINDOW"));
                add((String) locators.get("JOURNAL_TAG"));
            }}, fileName);

        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    @Test
    public void test() {

        Browser.getInstance().open(url);

        Assert.assertTrue(
                pageUtils.findElement(
                        true, authPage.getPassportLocator()).isEnabled());

        authPage.login();

        Assert.assertTrue(
                pageUtils.findElement(
                        false, mainPage.getLogoutForm()).isEnabled());

        mainPage.clickRandomCategory();

        Assert.assertEquals(mainPage.getCategoryUrl(), Browser.getInstance().getDriver().getCurrentUrl());

        mainPage.clickCatalogue();
        List<List<String>> allCategories = mainPage.allCategories();

        try {
            List<String> result = Arrays.asList(FileUtils.readCsv(mainPage.getFileName()).get(0));
            List<String> categories = allCategories.get(0);
            Assert.assertTrue(categories.containsAll(result) && result.containsAll(categories));
        } catch (IOException | com.opencsv.exceptions.CsvException ex) {
            System.err.println(ex);
        }

        Assert.assertTrue(mainPage.getAllCategoriesUrl().containsAll(
                mainPage.getPopularCategoriesUrl()));

        mainPage.logout();
        Assert.assertTrue(
                pageUtils.findElement(
                        true, authPage.getPassportLocator()).isEnabled());

        Browser.getInstance().driverQuit();
    }
}

import com.google.common.base.Function;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import java.time.Duration;
import java.util.List;

public class PageUtils {

    private static long timeoutSeconds;
    private static long pollingSeconds;

    private final WebDriver DRIVER;
    private final Wait<WebDriver> WAIT;

    public static void setTimeoutSeconds(long _timeoutSeconds) {
        timeoutSeconds = _timeoutSeconds;
    }

    public static void setPollingSeconds(long _pollingSeconds) {
        pollingSeconds = _pollingSeconds;
    }
    public PageUtils() {
        this.DRIVER = Browser.getInstance().getDriver();
        WAIT = new FluentWait<>(DRIVER)
                .withTimeout(Duration.ofSeconds(timeoutSeconds))
                .pollingEvery(Duration.ofSeconds(pollingSeconds))
                .ignoring(NoSuchElementException.class);
    }

    public WebElement findElement(final boolean XPathWay, final String selector) {
        WebElement element = WAIT.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                return (XPathWay) ?
                        driver.findElement(By.xpath(selector)) : driver.findElement(By.cssSelector(selector));
            }
        });
        return element;
    }

    public void clickElement(WebElement element) {
        try {
            element.click();
        } catch (ElementClickInterceptedException | ElementNotVisibleException ex) {
            findElement(true, MainPage.getPopUpWindow()).click();
            System.out.println("!!! spy was visible !!!");
            element.click();
        }
    }

    public List<WebElement> findElements(String selector) {
        return DRIVER.findElements(By.xpath(selector));
    }
}

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import file_utils.FileUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainPage extends PageUtils {

    private static final String TEXT_CONTENT = "textContent";
    private static final String HREF = "href";
    private static String popUpWindow;

    private final WebDriver DRIVER = Browser.getInstance().getDriver();

    private final String MARKET_APP_TAG;
    private final String FILENAME;
    private final String DISCOUNT_TAG;
    private final String GOODS_BUTTON;
    private final String ALL_GOODS;
    private final String DOSUG;
    private final String POPULAR_GOODS;
    private final String LOGOUT_FORM;
    private final String LOGOUT_BUTTON;
    private final String JOURNAL_TAG;

    private List<String> popularCategoriesUrl = new ArrayList<>();
    private List<String> allCategoriesUrl = new ArrayList<>();

    private String categoryUrl;

    public MainPage(ArrayList<String> locators, String fileName) {
        MARKET_APP_TAG = locators.get(0);
        DISCOUNT_TAG = locators.get(1);
        GOODS_BUTTON = locators.get(2);
        ALL_GOODS = locators.get(3);
        DOSUG = locators.get(4);
        POPULAR_GOODS = locators.get(5);
        LOGOUT_FORM = locators.get(6);
        LOGOUT_BUTTON = locators.get(7);
        popUpWindow = locators.get(8);
        JOURNAL_TAG = locators.get(9);
        FILENAME = fileName;
    }

    public void clickCatalogue() {
        clickElement(findElement(true, GOODS_BUTTON));
    }

    public List<List<String>> allCategories() {

        findElement(true, ALL_GOODS);
        List<List<String>> data = new ArrayList<>();
        data.add(new ArrayList<String>());

        List<WebElement> catalog = findElements(ALL_GOODS);
        for (int i = 1; i <= catalog.size(); i++) {
            try {
                data.get(0).add(catalog.get(i - 1).getAttribute(TEXT_CONTENT));
                allCategoriesUrl.add(catalog.get(i - 1).getAttribute(HREF));
            } catch (org.openqa.selenium.StaleElementReferenceException ex) {
                WebElement element = DRIVER.findElement(By.xpath(String.format("(" + ALL_GOODS + ")" + "[%d]", i)));
                data.get(0).add(element.getAttribute(TEXT_CONTENT));
                allCategoriesUrl.add(element.getAttribute(HREF));
            }
        }
        try {
            FileUtils.writeCsv(data, FILENAME);
        } catch (IOException ex) {
            System.err.println(ex);
        }
        return data;
    }

    public ArrayList<WebElement> popularCategories() {

        findElement(true, DOSUG);
        List<WebElement> allCategories = findElements(POPULAR_GOODS);
        ArrayList<WebElement> goods = new ArrayList<>();

        for (int i = 1; i <= allCategories.size(); i++) {
            try {
                if (isGoodCategory(allCategories.get(i - 1))) {
                    goods.add(allCategories.get(i - 1));
                    popularCategoriesUrl.add(allCategories.get(i - 1).getAttribute(HREF));
                }
            } catch (org.openqa.selenium.StaleElementReferenceException ex) {
                WebElement category = DRIVER.findElement(By.xpath(
                        String.format("(" + POPULAR_GOODS + ")" + "[%d]", i)));
                if (isGoodCategory(category)) {
                    goods.add(category);
                    popularCategoriesUrl.add(category.getAttribute(HREF));
                }
            }
        }
        return goods;
    }

    public void clickRandomCategory() {
        WebElement element = popularCategories().get(chooseRandomNumber(popularCategoriesUrl.size() - 1));
        categoryUrl = element.getAttribute(HREF);
        clickElement(element);
    }

    public void logout() {
        clickElement(findElement(false, LOGOUT_FORM));
        new Actions(DRIVER).moveToElement(
                findElement(true, LOGOUT_BUTTON)).click().perform();
    }

    private boolean isGoodCategory(WebElement element) {
        return !(element.getAttribute(HREF).contains(MARKET_APP_TAG) ||
                element.getAttribute(HREF).contains(DISCOUNT_TAG) ||
                element.getAttribute(HREF).contains(JOURNAL_TAG));
    }

    private static int chooseRandomNumber(int countCategories) {
        Random random = new Random();
        return random.nextInt(countCategories);
    }

    public List<String> getPopularCategoriesUrl() {
        return popularCategoriesUrl;
    }

    public List<String> getAllCategoriesUrl() {
        return allCategoriesUrl;
    }

    public String getFileName() {
        return FILENAME;
    }

    public String getLogoutForm() {
        return LOGOUT_FORM;
    }
    public String getCategoryUrl() {
        return categoryUrl;
    }

    public static String getPopUpWindow() {
        return popUpWindow;
    }
}

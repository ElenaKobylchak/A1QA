import browsers.*;

public class BrowserFactory {

    private final String NAME;

    private Driver driver;

    public enum BrowserChoice {
        CHROME, FIREFOX
    }

    public BrowserFactory(String name) {
        this.NAME = name;
    }

    public void createBrowser() {
        switch (BrowserChoice.valueOf(NAME)){
            case CHROME:
                driver = new Chrome();
                break;
            case FIREFOX:
                driver = new Firefox();
                break;
        }
        Browser.getInstance().setDriver(driver.getDriver());
    }
}

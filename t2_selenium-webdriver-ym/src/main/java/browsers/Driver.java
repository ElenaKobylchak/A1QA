package browsers;

import org.openqa.selenium.WebDriver;

public interface Driver {

    WebDriver getDriver();
}

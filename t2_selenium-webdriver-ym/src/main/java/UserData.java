public class UserData {

    private final String LOGIN;
    private final String PASSWORD;

    public UserData(String _login, String _password) {
        LOGIN = _login;
        PASSWORD = _password;
    }

    public String getLogin() {
        return LOGIN;
    }

    public String getPassword() {
        return PASSWORD;
    }
}

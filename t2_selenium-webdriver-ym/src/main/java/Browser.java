import org.openqa.selenium.WebDriver;

public class Browser {

    private static Browser instance;
    private WebDriver driver;

    public static synchronized Browser getInstance() {
        if (instance == null) {
            instance = new Browser();
        }
        return instance;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public void open(String url) {
        driver.get(url);
    }

    public void switchToNewWindow() {
        for(String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
    }

    public void driverQuit() {
        driver.quit();
    }

    public WebDriver getDriver() {
        return driver;
    }

}

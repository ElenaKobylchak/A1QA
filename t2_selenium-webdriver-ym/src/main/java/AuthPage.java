import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.ArrayList;

public class AuthPage extends PageUtils {

    private final WebDriver DRIVER = Browser.getInstance().getDriver();

    private final String LOGIN;
    private final String PASSWORD;

    private final String LOGIN_LABEL;
    private final String LOGIN_LOCATOR;
    private final String PASSWORD_LOCATOR;
    private final String FIELD_FILLED;
    private final String PASSPORT_LOCATOR;

    public AuthPage(UserData userData, ArrayList<String> locators) {
        this.LOGIN = userData.getLogin();
        this.PASSWORD = userData.getPassword();
        this.LOGIN_LABEL = locators.get(0);
        this.LOGIN_LOCATOR = locators.get(1);
        this.PASSWORD_LOCATOR = locators.get(2);
        this.FIELD_FILLED = locators.get(3);
        this.PASSPORT_LOCATOR = locators.get(4);
    }

    public void enterLogin() {
        WebElement loginField = findElement(false, LOGIN_LABEL);
        loginField.click();
        WebElement login = findElement(false, LOGIN_LOCATOR);
        login.sendKeys(LOGIN);
        if (findElement(true, FIELD_FILLED).isEnabled()) {
            login.sendKeys(Keys.ENTER);
        }
    }

    public void enterPassword() {
        WebElement passwordField = findElement(false, PASSWORD_LOCATOR);
        passwordField.sendKeys(PASSWORD);
        if (findElement(true, FIELD_FILLED).isEnabled()) {
            passwordField.sendKeys(Keys.ENTER);
        }
    }

    public void login() {
        String winHandleBefore = DRIVER.getWindowHandle();
        clickElement(findElement(true, PASSPORT_LOCATOR));
        Browser.getInstance().switchToNewWindow();
        enterLogin();
        enterPassword();
        DRIVER.switchTo().window(winHandleBefore);
    }

    public String getPassportLocator() {
        return PASSPORT_LOCATOR;
    }
}

package triangle;

import java.util.Comparator;

public class ComparatorByOX implements Comparator<Point>{
    
    @Override
    public int compare(Point x, Point y) {        
        if (x.getY() > y.getY()) {
            return 1;
        }
        if (x.getY() < y.getY()) {
            return -1;
        }
        return 0;
    }
}
package triangle;

import java.util.Arrays;

public final class Triangle {

    public Triangle (String str) {
        this(str.split(" "));
    }

    private Triangle(String[] splitted) {
        this(
            new Point(Double.parseDouble(splitted[0]), Double.parseDouble(splitted[1])),
            new Point(Double.parseDouble(splitted[2]), Double.parseDouble(splitted[3])),
            new Point(Double.parseDouble(splitted[4]), Double.parseDouble(splitted[5])));
    }
    
    private static boolean isNotTriangle(Point a, Point b, Point c) {
        return ((a.getX() ==  b.getX() && b.getX() == c.getX()) || (a.getY() == b.getY() && b.getY() == c.getY()) ||
               ((b.getY() - a.getY()) * (c.getX() - a.getX()) == (b.getX() - a.getX()) * (c.getY() - a.getY())));
    }

    private static String pointsArrayToString(Point[] points) {
        String result = "";
        for (Point point : points) {
            result += point + " ";
        }
        return result;
    }

    private final Point a, b, c;
    private final double AB, BC, AC;
    
    public Triangle (Point _a, Point _b, Point _c) {
        a = _a;
        b = _b;
        c = _c;
        AB = Point.distance(a, b);
        BC = Point.distance(b, c);
        AC = Point.distance(a, c);

        if (isNotTriangle(a, b, c)) {
            throw new IllegalArgumentException("It's not a triangle. Points: " +
                      pointsArrayToString(new Point[] {a, b, c}));
           }
    }
    
    @Override
    public String toString() {
        String result = "Points: " + pointsArrayToString(new Point[] {a, b, c});
        if (isRectangular()) {
            result += "\nRight angle point coordinates: " + rightAngleCoordinates() +
                            "\nArea: " + area() + "\nClosest to OX: ";
            result += pointsArrayToString(coodinatesClosestOX()) + "\n";
            return result;
        }
        return result + "\nTriangle is not rectangular\n";
    }


    private double area() {
        double p = (AB + BC + AC) / 2;
        return Math.sqrt(p * ((p - AB) * (p - BC) * (p - AC)));
    }

    private boolean isRectangular() {
        double aB = Math.pow(AB, 2);
        double bC = Math.pow(BC, 2);
        double aC = Math.pow(AC, 2);

        return aB == bC + aC || bC == aB + aC || aC == bC + aB;
    }

    private Point rightAngleCoordinates() {
        if (AB > BC && AB > AC) {
            return c;
        }
        if (BC > AC && BC > AB) {
            return a;
        }
        return b;
    }

    private Point[] coodinatesClosestOX () {
        ComparatorByOX comparator = new ComparatorByOX();

        Point[] allPoints = {a, b, c};
        Arrays.sort(allPoints, comparator);
        
        if (comparator.compare(allPoints[0], allPoints[1]) == 0) {
            return new Point[] {allPoints[0], allPoints[1]};
        }
        return new Point[] {allPoints[0]};
    }
}
package day;

public final class Minutes implements Comparable<Minutes>{

    private final int value;
    
     /**
     * Constructor of class Minutes.
     *
     * @param _value minutes value from 0 to 59
     */
    public Minutes(int _value) {
        if (_value < 0 || _value > 59) { 
            throw new IllegalArgumentException("Invalid minutes value");
        }
        value = _value;
    }

     /**
     * Getter for minutes value.
     * 
     * @return minutes value
     */
    public int getMinute() {
        return value;
    }

     /**
     * Object hashCode.
     * 
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return value;
    }

     /**
     * Compare by value.
     * 
     * @param obj Object to compare
     * @return true if same values
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Minutes other = (Minutes) obj;
        return other.value == value;
    }

     /**
     * String representation.
     * 
     * @return string reprepresentation
     */
    @Override
    public String toString() {
        return value + "min";
    }

     /**
     * Compare by value.
     * 
     * @param other Object to compare
     * @return int value - result of comparison
     */
    @Override
    public int compareTo(Minutes other) {
        return Integer.compare(value, other.value);
    }
}
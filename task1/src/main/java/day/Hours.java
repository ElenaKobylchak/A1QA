package day;

public final class Hours implements Comparable<Hours> {
    
    private final int value;

     /**
     * Constructor of class Hours.
     *
     * @param _value minutes value from 0 to 23
     */
    public Hours(int _value) {
        if (_value < 0 || _value > 23){
            throw new IllegalArgumentException("Invalid hours value");
        }  
        value = _value;
    }

     /**
     * Getter for hours value.
     * 
     * @return minutes value
     */
    public int getHour() {
        return value;
    }

     /**
     * Object hashCode.
     * 
     * @return hashcode
     */

    @Override
    public int hashCode() {
        return value;
    }
    
     /**
     * Compare by value.
     * 
     * @param obj Object to compare
     * @return true if same values
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Hours other = (Hours) obj;
        return other.value == value;
    }

     /**
     * String representation.
     * 
     * @return string reprepresentation
     */
    @Override
    public String toString() {
        return value + "h";
    }
    
     /**
     * Compare by value.
     * 
     * @param other Object to compare
     * @return int value - result of comparison
     */
    @Override
    public int compareTo(Hours other) {
        return Integer.compare(value, other.value);
    }
}
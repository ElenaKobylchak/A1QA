package day;

public final class Day implements Comparable<Day> {

    private final Hours hour;
    private final Minutes minute;

     /**
     * Constructor of class Day.
     *
     * @param _hour instance of class Hours
     * @param _minute instance of class Minutes
     */
    public Day(Hours _hour, Minutes _minute) {
        hour = _hour;
        minute = _minute;
    }

     /**
     * String representation.
     * 
     * @return string reprepresentation
     */
    @Override
    public String toString() {
        return hour + " " + minute;
    }

     /**
     * Object hashCode.
     * 
     * @return hashcode
     */
    @Override
    public int hashCode() {
        final int prime = 31;
		int result = 1;
		result = prime * result + hour.hashCode();
		result = prime * result + minute.hashCode();
		return result;
    }

     /**
     * Compare by value.
     * 
     * @param obj Object to compare
     * @return true if same values
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Day other = (Day) obj;
        return other.hour.equals(hour) && other.minute.equals(minute);
    }

     /**
     * Compare by value.
     * 
     * @param other Object to compare
     * @return int value - result of comparison
     */
    @Override
    public int compareTo(Day other) {
        if (hour.compareTo(other.hour) == 0) {
            return minute.compareTo(other.minute);
        }
        return hour.compareTo(other.hour);
    }

     /**
     * prints information about time
     */
    public void printCurrentTime() {
        System.out.println(String.format("Current time: %s\nTime of day : %s", this, getDayTime()));
    }

     /**
     * get information about day time
     * @return string of day time
     */
    public String getDayTime() {
        int h = hour.getHour();
        final int MORNING_STARTS = 6;
        final int DAY_STARTS = 11;
        final int EVENING_STARTS = 17;
        final int NIGHT_STARTS = 22;

        if (h >= MORNING_STARTS && h < DAY_STARTS) {
            return "morning";
        }
        if (h >= DAY_STARTS && h < EVENING_STARTS) {
            return "day";
        }
        if (h >= EVENING_STARTS && h < NIGHT_STARTS) {
            return "evening";
        }
        return "night";
    }
}
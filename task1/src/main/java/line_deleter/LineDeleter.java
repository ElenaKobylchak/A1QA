package line_deleter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import file_utils.FileUtils;



public class LineDeleter {

    private int start, end;
    private String name;

    public LineDeleter(String _name, int _start, int _end) {
        name = _name;
        start = _start;
        end = _end;
    }

    public void rewrite() {
        
        File inputFile = new File(name);
        File tempFile = new File(name + ".old");

        try (LineNumberReader reader = new LineNumberReader(new FileReader(inputFile));
            BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile))) {

            String line;
            while ((line = reader.readLine()) != null) {
                checkLine(writer, line, reader.getLineNumber());
            }

            if (FileUtils.endsWithNewLine(inputFile)) {
                writer.write("\n");
            }

        } catch (IOException ex) {
            System.err.println(ex);
            return;
        }
        
        inputFile.delete();
        tempFile.renameTo(inputFile);
    }
    
    
    private void checkLine(BufferedWriter writer, String line, int lineNumber) throws IOException {
        
        if (lineNumber >= start && lineNumber <= end) {
            System.out.println(line);
        }
        else {
            if (lineNumber != 1) {
                writer.write("\n");
            }
            writer.write(line);
            }
        }    
}
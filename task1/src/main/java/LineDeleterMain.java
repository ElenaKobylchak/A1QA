import line_deleter.LineDeleter;

public class LineDeleterMain {
    public static void main(String[] args) {

        try {
            new LineDeleter(args[0], Integer.parseInt(args[1]), Integer.parseInt(args[2])).rewrite();
        } catch (Exception ex) {
            System.out.println("Usage: TextFile <filename: String> <first_line: int> <second_line: int>");
        }
    }
}
import java.util.List;
import triangle.*;
import file_utils.FileUtils;
import file_utils.StringConstructible;


public class TriangleMain {

     /**
     * Use triangle.txt for example.
     */
    public static void main(String[] args) {
        try {
            List<Triangle> triangles = FileUtils.createObjectList(args[0], new StringConstructible<Triangle>() {
 
                @Override
                public Triangle createObject(String str) {
                    return new Triangle(str);
                }});
        
            for (Triangle triangle : triangles) {
                System.out.println(triangle);
            }
        } catch (Exception ex) {
            System.err.println("Usage: TextFile <filename: String>. Points for each triangle must be written with a space");
        }
    }

}
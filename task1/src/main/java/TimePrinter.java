
import java.time.LocalTime;
import day.*;



public class TimePrinter {
    public static void main(String[] args) {
        testHours();
        testMinutes();
        testDay();
    }

     /**
     * Test working of class Hours methods.
     */
    private static void testHours() {
        Hours hours5 = new Hours(5);
        System.out.println(hours5);
        System.out.println(hours5.equals(new Hours(5)));
        System.out.println(hours5.equals(new Hours(6)));
        System.out.println(hours5.compareTo(new Hours(6)));

        try{
            Hours h = new Hours(50);
        } catch (IllegalArgumentException ex) {
            System.out.println(ex);
        }   
    }

     /**
     * Test working of class Minutes methods.
     */
    private static void testMinutes() {
        Minutes min59 = new Minutes(59);
        System.out.println(min59); 
        System.out.println(min59.equals(new Minutes(59)));
        System.out.println(min59.equals(new Minutes(25)));
        System.out.println(min59.compareTo(new Minutes(25)));
        
        try{
            Minutes m = new Minutes(80);
        } catch (IllegalArgumentException ex) {
            System.out.println(ex);
        }
    }
     /**
     * Test working of class Day methods.
     */
    private static void testDay() {
        Hours hour = new Hours(5);
        Minutes minute = new Minutes(30);
        Day day = new Day(hour, minute);
        System.out.println(day);
        System.out.println(day.equals(new Day(hour, minute)));
        System.out.println(day.equals(new Day(new Hours(6), new Minutes(31))));
        System.out.println(day.compareTo(new Day(new Hours(6), new Minutes(31))));

        testCurrentTime();      
    }
     /**
     * Test getting current time.
     */
    private static void testCurrentTime() {
        LocalTime time = LocalTime.now();
        Hours hour = new Hours(time.getHour());
        Minutes minute = new Minutes(time.getMinute());
        Day day = new Day(hour, minute);
        day.printCurrentTime();
    }
}
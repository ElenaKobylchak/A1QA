import org.testng.Assert;
import org.testng.annotations.*;
import exception.FileNameAlreadyExistsException;

public class FileTest {

    private final FileStorage fileStorage = new FileStorage();
    private final String fileName = "fileName";
    private final String content = generateString(100);
    private final File testFile =  new File(fileName, content);;

    private static String generateString(int len) {
        char a = 'a';
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(a);
        return sb.toString();
    }

    @DataProvider(name="data-provider")
    public Object[][] dataProviderMethod() {
        return new Object[][] {{content}, {""}};
    }

    @Test(priority=0)
    private void testGetFilename() {
        Assert.assertEquals(testFile.getFilename(), fileName);
    }

    @Test(priority=1, dataProvider = "data-provider")
    private void testGetSize(String content) {
        File file = new File(fileName, content);
        Assert.assertEquals(file.getSize(), (double)content.length());
    }

    @Test(priority=2)
    private void testIsNotExists() {
        Assert.assertFalse(fileStorage.isExists(fileName));
    }

    @Test(priority=3)
    private void testWrite() throws FileNameAlreadyExistsException {
        Assert.assertTrue(fileStorage.write(testFile));
    }

    @Test(priority=4, expectedExceptions = FileNameAlreadyExistsException.class)
    private void testFileNameAlreadyExistsException() throws FileNameAlreadyExistsException {
        fileStorage.write(testFile);
    }

    @Test(priority=5)
    private void testAvailableSizeLimit1() throws FileNameAlreadyExistsException {
        File newFile = new File("newName", content);
        Assert.assertTrue(fileStorage.write(newFile));
    }

    @Test(priority=6)
    private void testAvailableSizeLimit2() throws FileNameAlreadyExistsException {
        File newFile = new File("newName", generateString(101));
        Assert.assertFalse(fileStorage.write(newFile));
    }

    @Test(priority=7)
    private void testAvailableSizeLimit3() throws FileNameAlreadyExistsException {
        File newFile = new File("newName", generateString(20));
        Assert.assertTrue(fileStorage.write(newFile));
    }

    @Test(priority=8)
    private void testIsExists() {
        Assert.assertTrue(fileStorage.isExists(fileName));
    }

    @Test(priority=9)
    private void testGetFile() {
        Assert.assertEquals(fileStorage.getFile(fileName), testFile);
    }

    @Test(priority=10)
    private void testGetFiles() {
        Assert.assertEquals(fileStorage.getFiles().get(0), testFile);
    }

    @Test(priority=11)
    private void testDelete() {
        Assert.assertTrue(fileStorage.delete(fileName));
    }

    @Test(priority=12)
    private void testNotExists() {
        testIsNotExists();
    }

    @Test(priority=13)
    private void testGetDeletedFile() {
        Assert.assertEquals(fileStorage.getFile(fileName), null);
    }

    @Test(priority=14)
    private void testGetDeletedFiles() {
        Assert.assertNotEquals(fileStorage.getFiles().get(0), testFile);
    }

    @Test(priority=15)
    private void testDeleteAgain() {
        Assert.assertFalse(fileStorage.delete(fileName));
    }
}
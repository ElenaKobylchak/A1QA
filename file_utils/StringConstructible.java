package file_utils;

public interface StringConstructible <T>{

    public T createObject(String str);

}
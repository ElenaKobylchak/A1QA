package file_utils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import static java.nio.charset.StandardCharsets.UTF_8;

public class FileUtils {

    public static <T> List<T> createObjectList(String fileName, file_utils.StringConstructible<T> factory) throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
        ArrayList<T> objects = new ArrayList<>();

        for (String line : lines) {
            try {
                objects.add(factory.createObject(line));

            } catch (IllegalArgumentException ex) {
                System.err.println(ex + "\n");
            }
        }
        return objects;
    }

    public static boolean endsWithNewLine(File file) throws IOException {
        try (RandomAccessFile fileHandler = new RandomAccessFile(file, "r")) {
            long fileLength = fileHandler.length() - 1;

            if (fileLength < 0) {
                return false;
            }
            fileHandler.seek(fileLength);
            byte readByte = fileHandler.readByte();

            return readByte == 0xA || readByte == 0xD;
        }
    }

    public static void writeCsv(List<List<String>> stringArray, String fileName) throws IOException {
        try (CSVWriter writer = new CSVWriter(new FileWriter(fileName, UTF_8))) {
            for (List<String> list : stringArray) {
                writer.writeNext(list.toArray(new String[list.size()]));
            }
        }
    }

    /*
     * method returns List<String[]>, needs Array.asList() for each list.
     * example: Arrays.asList(res.get(0)).
     */
    public static List<String[]> readCsv(String fileName) throws IOException, com.opencsv.exceptions.CsvException {
        try (CSVReader reader = new CSVReader(new FileReader(fileName, UTF_8))) {
            return reader.readAll();
        }
    }

    public static JSONArray readJSON(String fileName) throws IOException, org.json.simple.parser.ParseException {
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader(fileName, UTF_8)) {
            Object obj = jsonParser.parse(reader);
            return (JSONArray) obj;
        }
    }

    public static Object readConfig(String fileName, String key) throws IOException, org.json.simple.parser.ParseException {
        JSONObject data = (JSONObject) readJSON(fileName).get(0);
        return data.get(key);
    }

    public static void writeExtraLine(String path, String extraLine, int position) throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(path), StandardCharsets.UTF_8);
        lines.add(position, extraLine);
        Files.write(Paths.get(path), lines, UTF_8);
    }
}

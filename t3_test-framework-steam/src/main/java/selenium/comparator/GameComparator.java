package selenium.comparator;

import selenium.elements.Game;
import java.util.Comparator;

public class GameComparator implements Comparator<Game> {

    @Override
    public int compare(Game game1, Game game2) {
        String text1 = game1.getDiscount();
        String text2 = game2.getDiscount();
        int value1 = Integer.parseInt(
                     slice_range(text1, 1, text1.length() - 1));

        int value2 = Integer.parseInt(
                     slice_range(text2, 1, text1.length() - 1));

        return value1 < value2 ? -1 : value1 == value2 ? 0: 1;
    }

    private static String slice_range(String s, int startIndex, int endIndex) {
        if (startIndex < 0) startIndex = s.length() + startIndex;
        if (endIndex < 0) endIndex = s.length() + endIndex;
        return s.substring(startIndex, endIndex);
    }
}



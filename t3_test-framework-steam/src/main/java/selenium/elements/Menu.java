package selenium.elements;

public class Menu {

    private final String BUTTON;

    public Menu(String button) {
        BUTTON = button;
    }

    public void selectItem(String subButton) {
        new BaseElement(BUTTON).moveToElement(subButton);
    }
}

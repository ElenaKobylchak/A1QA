package selenium.elements;

import java.time.Duration;
import java.util.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.apache.log4j.Logger;
import com.google.common.base.Function;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import selenium.browsers.Browser;
import java.util.List;

public class BaseElement {

    private static final Logger LOG = Logger.getLogger(BaseElement.class);
    private static final String TEXT_CONTENT = "textContent";
    private static long timeoutSeconds;
    private static long pollingSeconds;

    private final String LOCATOR;

    public BaseElement(String locator) {
        this.LOCATOR = locator;
    }

    public static void setTimeoutSeconds(long _timeoutSeconds) {
        timeoutSeconds = _timeoutSeconds;
    }

    public static void setPollingSeconds(long _pollingSeconds) {
        pollingSeconds = _pollingSeconds;
    }

    public WebElement findElement() {
        Wait<WebDriver> wait = new FluentWait<>(Browser.getDriver())
                .withTimeout(Duration.ofSeconds(timeoutSeconds))
                .pollingEvery(Duration.ofSeconds(pollingSeconds))
                .ignoring(org.openqa.selenium.NoSuchElementException.class)
                .ignoring(NoSuchElementException.class);

        WebElement element = wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {

                return driver.findElement(By.xpath(LOCATOR));
            }
        });
        return element;
    }

    public WebElement getElement() {
        WebElement element;
        try {
            element =  findElement();
            LOG.info("element was found");
        } catch (Exception ex) {
            element = null;
            LOG.error(ex);
        }
        return element;
    }

    public void clickElement() {
        try {
            getElement().click();
            LOG.info("element was clicked");
        } catch (Exception ex) {
            LOG.error(ex);
        }
    }

    public List<WebElement> findElements() {
        return Browser.getDriver().findElements(By.xpath(LOCATOR));
    }

    public WebElement findElementWithoutWaiting() {
        return Browser.getDriver().findElement(By.xpath(LOCATOR));
    }

    public static String getTextContent(WebElement element) {
        return element.getAttribute(TEXT_CONTENT);
    }

    public String getTextContent() {
        return getElement().getAttribute(TEXT_CONTENT);
    }

    public void moveToElement(String secondLocator) {
        Actions a = new Actions(Browser.getDriver());
        WebElement we = getElement();
        a.moveToElement(we).perform();
        new BaseElement(secondLocator).clickElement();
    }
}


package selenium.elements;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import selenium.comparator.GameComparator;

public class GamesTable {

    private static final Logger LOG = Logger.getLogger(GamesTable.class);

    private final String FIRST_GAME_WITH_DISCOUNT;
    private final String ALL_GAMES_NAMES;
    private final String DISCOUNT_PCT;
    private final String GAME_NAME;
    private final String ORIGINAL_PRICE;
    private final String FINAL_PRICE;

    public GamesTable(ArrayList<String> locators) {
        FIRST_GAME_WITH_DISCOUNT = locators.get(0);
        ALL_GAMES_NAMES = locators.get(1);
        DISCOUNT_PCT = locators.get(2);
        GAME_NAME = locators.get(3);
        ORIGINAL_PRICE = locators.get(4);
        FINAL_PRICE = locators.get(5);
    }

    public List<Game> findGames() {

        if (!getFirstDiscountGame().isEnabled()) {
            LOG.error("no games with discount");
            return null;
        }
        List<WebElement> namesElements = new BaseElement(ALL_GAMES_NAMES).findElements();
        List<Game> games = new ArrayList<>();

        for (int number =  0; number < namesElements.size(); number++) {
            try {
                games.add(new Game(
                        String.format(GAME_NAME, number + 1),
                        BaseElement.getTextContent(
                                new BaseElement(String.format(GAME_NAME, number + 1)).findElementWithoutWaiting()),
                        BaseElement.getTextContent(
                                new BaseElement(String.format(ORIGINAL_PRICE, number + 1)).findElementWithoutWaiting()),
                        BaseElement.getTextContent(
                                new BaseElement(String.format(FINAL_PRICE, number + 1)).findElementWithoutWaiting()),
                        BaseElement.getTextContent(
                                new BaseElement(String.format(DISCOUNT_PCT, number + 1)).findElementWithoutWaiting())));
            } catch (org.openqa.selenium.NoSuchElementException ex) {
                LOG.error("game without discount");
            }
        }

        Collections.sort(games, new GameComparator());
        return games;
    }

    private WebElement getFirstDiscountGame() {
        return new BaseElement(FIRST_GAME_WITH_DISCOUNT).getElement();
    }
}
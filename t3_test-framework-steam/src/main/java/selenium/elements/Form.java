package selenium.elements;

public class Form {

    private final String BUTTON;
    public Menu MENU;

    public Form(String button) {
        BUTTON = button;
        MENU = new Menu(BUTTON);
    }
}

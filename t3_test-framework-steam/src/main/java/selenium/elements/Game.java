package selenium.elements;

public class Game {

    private final String LOCATOR;
    private final String NAME;
    private final String ORIGINAL_PRICE;
    private final String FINAL_PRICE;
    private final String DISCOUNT;

    public Game(String locator, String name, String originalPrice, String discountPrice, String discount) {
        LOCATOR = locator;
        NAME = name;
        ORIGINAL_PRICE = originalPrice;
        FINAL_PRICE = discountPrice;
        DISCOUNT = discount;
    }

    public String getName() {
        return NAME;
    }

    public String getOriginalPrice() {
        return ORIGINAL_PRICE;
    }

    public String getDiscountPrice() {
        return FINAL_PRICE;
    }

    public String getLocator() {
        return LOCATOR;
    }

    public String getDiscount() {
        return DISCOUNT;
    }
}

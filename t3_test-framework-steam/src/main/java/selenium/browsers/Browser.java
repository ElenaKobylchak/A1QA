package selenium.browsers;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class Browser {

    private static final Logger LOG = Logger.getLogger(Browser.class);
    private static WebDriver driver;

    public Browser(WebDriver driver) {
        this.driver = driver;
    }

    public void open(String url) {
        LOG.info(url + " open");
        driver.get(url);
        driver.manage().window().maximize();
    }

    public void driverQuit() {
        LOG.info("close driver");
        driver.quit();
    }

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ex) {
            System.out.println(ex);
        }
    }

    public static WebDriver getDriver() {
        return driver;
    }
}
package selenium.browsers;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import java.util.HashMap;
import java.util.Map;

public class Chrome implements Driver {

    public WebDriver getDriver(String path) {
        ChromeOptions options = new ChromeOptions();

        Map<String, Object> prefs = new HashMap<>();
        prefs.put("safebrowsing.enabled", true);
        prefs.put("download.default_directory", path);
        prefs.put("disable-popup-blocking", true);
        options.setExperimentalOption("prefs", prefs);
        WebDriverManager.chromedriver().setup();

        return new ChromeDriver(options);
    }
}

package selenium.browsers;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;

public class Firefox implements Driver {

    public WebDriver getDriver(String path) {

        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.download.dir", path);
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/octet-stream");
        profile.setPreference("pdfjs.disabled", true);
        FirefoxOptions option=new FirefoxOptions();
        option.setProfile(profile);
        WebDriverManager.firefoxdriver().setup();

        return new FirefoxDriver(option);
    }
}

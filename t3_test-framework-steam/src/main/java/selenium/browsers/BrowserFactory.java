package selenium.browsers;

public class BrowserFactory {

    private final String NAME;
    private final String PATH;

    private Driver driver;
    private Browser browser;

    public enum BrowserChoice {
        CHROME, FIREFOX
    }

    public BrowserFactory(String name, String path) {
        this.NAME = name;
        this.PATH = path;
    }

    public void createBrowser() {
        switch (BrowserChoice.valueOf(NAME)) {
            case CHROME:
                driver = new Chrome();
                break;
            case FIREFOX:
                driver = new Firefox();
                break;
        }
        browser = new Browser(driver.getDriver(PATH));
    }

    public Browser getBrowser() {
        return browser;
    }

}

package selenium.pages;

import org.openqa.selenium.WebElement;
import selenium.browsers.Browser;
import selenium.elements.*;

public class InstallPage {

    private final String INSTALL_BUTTON_LOCATOR;
    private final BaseElement INSTALL_BUTTON;

    public InstallPage(String downloadButton) {
        INSTALL_BUTTON_LOCATOR = downloadButton;
        INSTALL_BUTTON = new BaseElement(INSTALL_BUTTON_LOCATOR);
    }

    public void installGame(long millis) {
        INSTALL_BUTTON.clickElement();
        Browser.sleep(millis);
    }

    public WebElement getInstallButton() {
        return INSTALL_BUTTON.getElement();
    }
}

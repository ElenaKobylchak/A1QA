package selenium.pages;

import org.openqa.selenium.WebElement;
import selenium.elements.BaseElement;
import selenium.elements.Game;
import selenium.elements.GamesTable;
import java.util.ArrayList;
import java.util.List;

public class GenrePage {

    private final ArrayList<String> GAMES_TABLE_LOCATORS;
    private final String TOP_SELLERS_TAB;
    private final String ACTIVE_TOP_SELLERS_TAB;

    private List<Game> gamesTable = new ArrayList<>();

    public GenrePage(ArrayList<String> gamesTableLocators, String topSellersTab, String activeTopSellersTab) {
        GAMES_TABLE_LOCATORS = gamesTableLocators;
        TOP_SELLERS_TAB = topSellersTab;
        ACTIVE_TOP_SELLERS_TAB = activeTopSellersTab;
    }

    public void selectTopSellersTab() {
        new BaseElement(TOP_SELLERS_TAB).clickElement();
    }

    public WebElement getActiveTopSellersTab() {
        return new BaseElement(ACTIVE_TOP_SELLERS_TAB).getElement();
    }

    public List<Game> findDiscountGames() {
        gamesTable = new GamesTable(GAMES_TABLE_LOCATORS).findGames();
        return gamesTable;
    }

    public Game selectGame(int index) {
        new BaseElement(gamesTable.get(index).getLocator()).clickElement();
        return gamesTable.get(index);
    }
}

package selenium.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import java.util.ArrayList;
import selenium.elements.*;

public class AgePage {

    private static final Logger LOG = Logger.getLogger(AgePage.class);

    private final String AGE_FORM;
    private final String SUBMIT;
    private final String AGE_DAY;
    private final String DAY;
    private final String AGE_MONTH;
    private final String MONTH;
    private final String AGE_YEAR;
    private final String YEAR;

    private final long DAY_VALUE;
    private final long MONTH_VALUE;
    private final long YEAR_VALUE;

    public AgePage(ArrayList<String> locators, long dayValue, long monthValue, long yearValue) {
        SUBMIT = locators.get(0);
        AGE_DAY = locators.get(1);
        DAY = locators.get(2);
        AGE_MONTH = locators.get(3);
        MONTH = locators.get(4);
        AGE_YEAR = locators.get(5);
        YEAR = locators.get(6);
        AGE_FORM = locators.get(7);
        DAY_VALUE = dayValue;
        MONTH_VALUE = monthValue;
        YEAR_VALUE = yearValue;
    }

    public void selectAge() {
        try {
            new BaseElement(AGE_FORM).findElementWithoutWaiting();
            setBirthday();
            LOG.info("age was selected");
        } catch (NoSuchElementException ex) {
            LOG.info("age was not required");
        }
    }

    private void setBirthday() {
        new BaseElement(AGE_DAY).moveToElement(String.format(DAY, DAY_VALUE));
        new BaseElement(AGE_MONTH).moveToElement(String.format(MONTH, MONTH_VALUE));
        new BaseElement(AGE_YEAR).moveToElement(String.format(YEAR, YEAR_VALUE));
        new BaseElement(SUBMIT).clickElement();
    }
}

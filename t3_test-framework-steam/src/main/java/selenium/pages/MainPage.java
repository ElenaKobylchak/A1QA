package selenium.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import selenium.elements.*;
import java.util.ArrayList;

public class MainPage {

    private static final Logger LOG = Logger.getLogger(MainPage.class);

    private final Form FORM;
    private final String INSTALL_BUTTON;
    private final String GAMES_BUTTON;
    private final String LOCALIZATION;
    private final String GENRE_GAME_EN;
    private final String GENRE_GAME_RUS;
    private final String TITLE_GAME_EN;
    private final String TITLE_GAME_RUS;
    private final String CHANGE_LANGUAGE;
    private final String ENGLISH;
    private final String ELEMENT_IN_ENGLISH;

    private String pageTitleLocator;
    private String pageTitle;

    public enum Localization {
        EN, RU
    }

    public MainPage(ArrayList<String> locators, String localization) {
        LOCALIZATION = localization;
        INSTALL_BUTTON = locators.get(0);
        GAMES_BUTTON = locators.get(1);
        GENRE_GAME_EN = locators.get(2);
        GENRE_GAME_RUS = locators.get(3);
        TITLE_GAME_EN = locators.get(4);
        TITLE_GAME_RUS = locators.get(5);
        CHANGE_LANGUAGE = locators.get(6);
        ENGLISH = locators.get(7);
        ELEMENT_IN_ENGLISH = locators.get(8);
        FORM =  new Form(GAMES_BUTTON);
    }

    public void clickInstallGameButton() {
        new BaseElement(INSTALL_BUTTON).clickElement();
    }

    public WebElement getGamesButton() {
        return new BaseElement(GAMES_BUTTON).getElement();
    }

    public void selectButtonOnForm(String nameEng, String titleEng, String nameRus, String urlRus) {
        String buttonLocator = null;
        switch (Localization.valueOf(LOCALIZATION)) {

            case EN:
                buttonLocator = String.format(GENRE_GAME_EN, nameEng);
                pageTitleLocator = String.format(TITLE_GAME_EN, nameEng);
                pageTitle = titleEng;
                break;

            case RU:
                buttonLocator = String.format(GENRE_GAME_RUS,urlRus);
                pageTitleLocator = String.format(TITLE_GAME_RUS, nameRus);
                pageTitle = nameRus;
                break;
        }
        FORM.MENU.selectItem(buttonLocator);
    }

    public void setLocalization() {
        LOG.info("set localization");

        switch (Localization.valueOf(LOCALIZATION)) {
            case EN:
                new BaseElement(CHANGE_LANGUAGE).clickElement();
                new BaseElement(ENGLISH).clickElement();
                ckeckLanguage();
                break;
            case RU:
                break;
        }
    }

    private void ckeckLanguage() {
        new BaseElement(ELEMENT_IN_ENGLISH).getElement();
    }

    public String getTitleLocator() {
        return pageTitleLocator;
    }

    public String getTitle() {
        return pageTitle;
    }
}

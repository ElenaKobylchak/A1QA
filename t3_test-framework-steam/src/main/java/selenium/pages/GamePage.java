package selenium.pages;

import selenium.elements.BaseElement;
import java.util.ArrayList;

public class GamePage {

    private final String GAME_NAME;
    private final String DISCOUNT_PCT;
    private final String ORIGINAL_PRICE;
    private final String FINAL_PRICE;

    public GamePage(ArrayList<String> locators) {
        GAME_NAME = locators.get(0);
        DISCOUNT_PCT = locators.get(1);
        ORIGINAL_PRICE = locators.get(2);
        FINAL_PRICE = locators.get(3);
    }

    public String getGameName() {
        return new BaseElement(GAME_NAME).getTextContent();
    }

    public String getDiscount_pct() {
        return new BaseElement(DISCOUNT_PCT).getTextContent();
    }

    public String getOriginalPrice() {
        return new BaseElement(ORIGINAL_PRICE).getTextContent();
    }

    public String getFinalPrice() {
        return new BaseElement(FINAL_PRICE).getTextContent();
    }
}

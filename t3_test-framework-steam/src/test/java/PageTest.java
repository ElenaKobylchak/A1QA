import file_utils.FileUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.*;
import java.io.File;
import java.util.*;
import org.json.simple.JSONArray;
import selenium.browsers.BrowserFactory;
import selenium.elements.*;
import selenium.pages.*;

public class PageTest {

    private static final Logger LOG = Logger.getLogger(PageTest.class);

    private JSONArray locators;
    private JSONObject testData;

    private BrowserFactory browserFactory;
    private MainPage mainPage;
    private InstallPage installPage;
    private GenrePage genrePage;
    private AgePage agePage;
    private GamePage gamePage;

    private String currentPath = new File("").getAbsolutePath();
    private String browserName;
    private String url;
    private String downloadedFileName;
    private long downloadWaiting;

    @BeforeTest
    private void readConfigs() {
        try {
            url = (String) FileUtils.readConfig("config.json", "url");
            browserName = (String) FileUtils.readConfig("config.json", "browser");

        } catch (Exception ex) {
            LOG.error(ex);
        }
    }

    @BeforeTest
    private void readLocatorsAndTestData() {
        try {
            locators = FileUtils.readJSON("locators.json");
            testData = (JSONObject) FileUtils.readJSON("test_data.json").get(0);
            currentPath += ((String) testData.get("path"));
            downloadWaiting = (Long) testData.get("wait_for_download_millis");
            downloadedFileName = (String) testData.get("download_file");
            BaseElement.setTimeoutSeconds((Long) testData.get("timeout_seconds"));
            BaseElement.setPollingSeconds((Long) testData.get("polling_seconds"));
            createMainPage();
            createInstallPage();
            createGenrePage();
            createAgePage();
            createGamePage();
            
        } catch (Exception ex) {
            LOG.error(ex);
        }
    }

    @BeforeMethod
    private void logStartWrite() {
        LOG.info("start executing TestSuite");
    }

    @BeforeMethod
    private void openBrowser() {
        browserFactory = new BrowserFactory(browserName, currentPath);
        browserFactory.createBrowser();
        browserFactory.getBrowser().open(url);
    }

    @BeforeMethod
    private void setLocalization() {
        mainPage.setLocalization();
    }

    @Test
    public void testSuite1() {
        isMainPageOpen();
        mainPage.clickInstallGameButton();
        Assert.assertTrue(installPage.getInstallButton().isEnabled());
        installPage.installGame(downloadWaiting);
        Assert.assertTrue(isFileDownloaded(currentPath, downloadedFileName));
    }

    @Test
    public void testSuite2() {
        isMainPageOpen();
        selectGameGenre("action.json");
        isGenrePageOpen();
        testTopSellersTab();
        List<Game> games = genrePage.findDiscountGames();
        Game game = genrePage.selectGame(games.size() - 1);
        agePage.selectAge();
        testGame(game);
    }

    @Test
    public void testSuite3 () {
        isMainPageOpen();
        selectGameGenre("indie.json");
        isGenrePageOpen();
        testTopSellersTab();
        genrePage.findDiscountGames();
        Game game = genrePage.selectGame(0);
        agePage.selectAge();
        testGame(game);
    }

    @AfterMethod
    private void browserQuit() {
        browserFactory.getBrowser().driverQuit();
    }

    @AfterMethod
    private void endLogWrite() {
        LOG.info("end executing TestSuite");
    }

    private void testTopSellersTab() {
        genrePage.selectTopSellersTab();
        Assert.assertTrue(genrePage.getActiveTopSellersTab().isEnabled());
    }

    private void selectGameGenre(String fileName) {
        try {
            JSONObject locators = (JSONObject) FileUtils.readJSON(fileName).get(0);

            mainPage.selectButtonOnForm(
                    (String) locators.get("name_eng"),
                    (String) locators.get("title_eng"),
                    (String) locators.get("name_rus"),
                    (String) locators.get("url_rus"));

        } catch (Exception ex) {
            LOG.error(ex);
        }
    }
    private void isMainPageOpen() {
        Assert.assertTrue(mainPage.getGamesButton().isEnabled());
    }

    private void isGenrePageOpen() {
        Assert.assertTrue(
                new BaseElement(
                        mainPage.getTitleLocator()).getTextContent().contains(mainPage.getTitle()));
    }

    private void testGame(Game game) {
        Assert.assertEquals(gamePage.getGameName(), game.getName());
        Assert.assertEquals(gamePage.getDiscount_pct(), game.getDiscount());
        Assert.assertEquals(gamePage.getOriginalPrice(), game.getOriginalPrice());
        Assert.assertEquals(gamePage.getFinalPrice(), game.getDiscountPrice());
    }

    private static boolean isFileDownloaded(String downloadPath, String fileName) {
        File dir = new File(downloadPath);
        File[] dirContents = dir.listFiles();

        for (int i = 0; i < dirContents.length; i++) {
            if (dirContents[i].getName().equals(fileName)) {
                return true;
            }
        }
        return false;
    }

    private void createMainPage() {
        final JSONObject mainPageLocators = (JSONObject) locators.get(0);
        mainPage = new MainPage(new ArrayList<String>()
        {{
            add((String) mainPageLocators.get("install_button"));
            add((String) mainPageLocators.get("games_button"));
            add((String) mainPageLocators.get("genre_game_en"));
            add((String) mainPageLocators.get("genre_game_rus"));
            add((String) mainPageLocators.get("title_game_en"));
            add((String) mainPageLocators.get("title_game_rus"));
            add((String) mainPageLocators.get("change_language"));
            add((String) mainPageLocators.get("english"));
            add((String) mainPageLocators.get("element_in_english"));
        }}, (String) testData.get("localization"));
    }

    private void createInstallPage() {
        final JSONObject installPageLocators = (JSONObject) locators.get(1);
        installPage = new InstallPage((String) installPageLocators.get("download_button"));
    }

    private void createGenrePage() {
        final JSONObject genrePageLocators = (JSONObject) locators.get(2);
        genrePage = new GenrePage(new ArrayList<String>()
        {{
            add((String) genrePageLocators.get("first_game_with_discount"));
            add((String) genrePageLocators.get("all_games_names"));
            add((String) genrePageLocators.get("discount_pct"));
            add((String) genrePageLocators.get("game_name"));
            add((String) genrePageLocators.get("original_price"));
            add((String) genrePageLocators.get("final_price"));
        }}, (String) genrePageLocators.get("top_sellers_tab"),
            (String) genrePageLocators.get("active_top_sellers_tab"));
    }

    private void createAgePage() {
        final JSONObject agePageLocators = (JSONObject) locators.get(3);
        agePage = new AgePage(new ArrayList<String>()
        {{
            add((String) agePageLocators.get("age_form_submit"));
            add((String) agePageLocators.get("day_form"));
            add((String) agePageLocators.get("day"));
            add((String) agePageLocators.get("month_form"));
            add((String) agePageLocators.get("month"));
            add((String) agePageLocators.get("year_form"));
            add((String) agePageLocators.get("year"));
            add((String) agePageLocators.get("age_form"));
        }}, (Long) testData.get("age_day"),
                (Long) testData.get("age_month"),
                (Long) testData.get("age_year"));
    }

    private void createGamePage() {
        final JSONObject gamePageLocators = (JSONObject) locators.get(4);
        gamePage = new GamePage(new ArrayList<String>() {{
            add((String) gamePageLocators.get("game_name"));
            add((String) gamePageLocators.get("discount_pct"));
            add((String) gamePageLocators.get("original_price"));
            add((String) gamePageLocators.get("final_price"));
        }});
    }
}
